package agh.qa;

import agh.qa.utils.StringUtils;

import java.text.ParseException;

public class PeselParser {
    public static Pesel parse(String peselStr) throws ParseException {
        System.out.println("Parsing pesel " + peselStr);

        if(!StringUtils.hasOnlyDigits(peselStr))
            throw new ParseException("Provided string must contain only digits", 1);

        if(peselStr.length() != 11)
            throw new ParseException("Provided string has invalid length", 0);

        byte[] peselArr = StringUtils.toByteArray(peselStr);

        return new Pesel(peselArr);
    }
}

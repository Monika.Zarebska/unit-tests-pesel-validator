package agh.qa.utils;

public class StringUtils {
    public static boolean hasOnlyDigits(String text){
        return text.matches("\\d+");
    }

    public static byte[] toByteArray(String text){
        byte[] bytes = new byte[text.length()];

        for (int i = 0; i < text.length(); i++){
            bytes[i] = Byte.parseByte(text.substring(i, i+1));
        }

        return bytes;
    }
}
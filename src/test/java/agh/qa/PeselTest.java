package agh.qa;

import agh.qa.utils.StringUtils;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PeselTest {

    @DataProvider
    public Object[][] invalidDigitIndex() {
        return new Object[][]{
                {-1},
                {11},
        };
    }

    @Test(dataProvider = "invalidDigitIndex")
    public void returnMinusOneForInvalidIndex(int index) {
        String peselNumber = "99523106414";
        Pesel pesel = new Pesel(StringUtils.toByteArray(peselNumber));

        Assert.assertEquals(pesel.getDigit(index), -1);
    }
}
package agh.qa.utils;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class StringUtilsTest {

    @DataProvider
    public Object[][] stringNumberToBytes() {
        return new Object[][]{
                {"012345", new byte[]{0,1,2,3,4,5}},
                {"01234567890", new byte[]{0,1,2,3,4,5,6,7,8,9,0}},
                {"012345678901", new byte[]{0,1,2,3,4,5,6,7,8,9,0,1}},
                {"", new byte[0]},
        };
    }

    @Test(dataProvider = "stringNumberToBytes")
    public void changeToBytes(String number, byte[] byteNumber) {

        Assert.assertEquals(StringUtils.toByteArray(number), byteNumber);
    }

    @DataProvider
    public Object[][] checkIfOnlyDigits() {
        return new Object[][]{
                {"861212", true},
                {"3a3", false},
                {"xyz", false},
                {"3.3", false},
                {"-12", false},
                {"", false},
        };
    }

    @Test(dataProvider = "checkIfOnlyDigits")
    public void shouldCheckIfOnlyDigits(String number, boolean expectedResult) {

        Assert.assertEquals(StringUtils.hasOnlyDigits(number), expectedResult);
    }
}
package agh.qa;

import agh.qa.utils.StringUtils;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.LocalDate;

public class PeselExtractorTest {

    @DataProvider
    public Object[][] peselNumberWithExpectedSex() {
        return new Object[][]{
                {"00210134168", "Female"},
                {"96022957411", "Male"},
        };
    }

    @Test(dataProvider = "peselNumberWithExpectedSex")
    public void checkSex(String peselNumber, String expectedSex) {
        Pesel pesel = new Pesel(StringUtils.toByteArray(peselNumber));
        PeselExtractor extractor = new PeselExtractor(pesel);

        Assert.assertEquals(extractor.getSex(), expectedSex);
    }

    @DataProvider
    public Object[][] invalidPesel() {
        return new Object[][]{
                {"9602295741"},
                {"960229574"},
        };
    }

    @Test(dataProvider = "invalidPesel")
    public void shouldFailOnInvalidPesel(String peselNumber) {
        Pesel pesel = new Pesel(StringUtils.toByteArray(peselNumber));
        PeselExtractor extractor = new PeselExtractor(pesel);

        extractor.getSex();
    }

    @Test
    public void checkBirthDate() {
        String peselNumber = "99723120779";
        LocalDate expectedBirthDate = LocalDate.parse("2299-12-31");
        Pesel pesel = new Pesel(StringUtils.toByteArray(peselNumber));
        PeselExtractor extractor = new PeselExtractor(pesel);

        Assert.assertEquals(extractor.getBirthDate(), expectedBirthDate);
    }
}
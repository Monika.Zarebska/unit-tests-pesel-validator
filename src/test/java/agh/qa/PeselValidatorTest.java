package agh.qa;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PeselValidatorTest {

    @DataProvider
    public Object[][] validPeselsThruCenturies() {
        return new Object[][]{
                {"00810199183", true}, //po kolei lata dobre ze 01.01.
                {"00010147069", true},
                {"00210134168", true},
                {"00410107243", true},
                {"00610139004", true},
                {"99923130877", true}, //po kolei lata z 31.12
                {"99123181152", true},
                {"99323118031", true},
                {"99523106414", true},
                {"99723120779", true},
        };
    }

    @Test(dataProvider = "validPeselsThruCenturies")
    public void shouldRecognizeValidPeselsThruCenturies(String pesel, boolean shouldBeValid) {
        PeselValidator validator = new PeselValidator();

        Assert.assertEquals(validator.validate(pesel), shouldBeValid);
    }

    @DataProvider
    public Object[][] peselParserExceptions() {
        return new Object[][]{
                {"0061013900y", false}, //false format
                {"0061013900", false}, //too short
                {"006101390022", false}, //too long
                {"-1022292612", false}, //false format
                {"102.2292612", false}, //false format
        };
    }

    @Test(dataProvider = "peselParserExceptions")
    public void shouldRecognizedExceptionsFromPeselParser(String pesel, boolean shouldBeValid) {
        PeselValidator validator = new PeselValidator();

        Assert.assertEquals(validator.validate(pesel), shouldBeValid);
    }

    @DataProvider
    public Object[][] invalidPeselInPeselValidator() {
        return new Object[][]{
                {"00610139003", false}, //invalid check sum
                {"10222926122", false}, //invalid birth date
        };
    }

    @Test(dataProvider = "invalidPeselInPeselValidator")
    public void invalidPeselInPeselValidator(String pesel, boolean shouldBeValid) {
        PeselValidator validator = new PeselValidator();

        Assert.assertEquals(validator.validate(pesel), shouldBeValid);
    }

    @DataProvider
    public Object[][] validPeselsValidDayInMonth() {
        return new Object[][]{
                {"20222926122", true}, // rok przestepny
                {"88813140733", true}, //po kolei m-ce
                {"97822860770", true},
                {"02233182096", true},
                {"00443052413", true},
                {"01653133488", true},
                {"88863039621", true},
                {"95073193786", true},
                {"01283112895", true},
                {"01493075490", true},
                {"22703178457", true},
                {"64913046715", true},
                {"52123198935", true},
        };
    }

    @Test(dataProvider = "validPeselsValidDayInMonth")
    public void shouldValidDaysAndMonthsInPesel(String pesel, boolean shouldBeValid) {
        PeselValidator validator = new PeselValidator();

        Assert.assertEquals(validator.validate(pesel), shouldBeValid);
    }

    @DataProvider
    public Object[][] invalidPeselsInvalidDayInMonthWithValidsSumCotrol() {
        return new Object[][]{
                {"20223026128", false}, //rok przestepny
                {"88813240730", false}, //po kolei m-ce z blednym dniem
                {"97822960777", false},
                {"02233282093", false},
                {"00443152410", false},
                {"01653233485", false},
                {"88863139628", false},
                {"95073293783", false},
                {"01283212892", false},
                {"01493175497", false},
                {"22703278454", false},
                {"64913146712", false},
                {"52123298932", false},
                {"52133198936", false} // 13 miesiac
        };
    }

    @Test(dataProvider = "invalidPeselsInvalidDayInMonthWithValidsSumCotrol")
    public void shouldInvalidDaysAndMonthsInPesel(String pesel, boolean shouldBeValid) {
        PeselValidator validator = new PeselValidator();

        Assert.assertEquals(validator.validate(pesel), shouldBeValid);
    }
}